# Comics Chong 📖 ️ 

Tienda de comics con Login, Listado principal de los comics y un Backoffice con CRUD completo, además permite subir imágenes.
Pueden ver un ejemplo de algunas de las vistas del proyecto en las imagenes adjuntadas en este readme o sino en las imagenes comics_1.png, comics_2.png... adjuntadas en la raíz del proyecto.


## Construido con 🛠️

* [Laravel](https://laravel.com/docs/5.4) - Laravel
* [Artisan](https://laravel.com/docs/5.4/artisan) - Manejador de dependencias


## Autor ✒️


* **Kevin Chong B.** - *Desarollador Web Full Stack* - [keevinnc](https://gitlab.com/keevinnc)
* 
## Imagenes 

Login necesario para acceder a todas las partes del proyecto

![Imagen 1](https://gitlab.com/keevinnc/tienda_comics/raw/master/comics_4.png)

Página pública principal de la tienda 

![Imagen 1](https://gitlab.com/keevinnc/tienda_comics/raw/master/comics_1.png)

Backoffice 

![Imagen 1](https://gitlab.com/keevinnc/tienda_comics/raw/master/comics_2.png)


![Imagen 1](https://gitlab.com/keevinnc/tienda_comics/raw/master/comics_3.png)
